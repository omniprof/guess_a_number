package com.cejv416.game;

/**
 * GameBean creates the secret number every time it is instantiated and stores
 * the number that is guessed
 *
 * @author Ken
 */
public class GameBean {

    private int secretNumber;
    private int guessNumber;

    public GameBean() {
        guessNumber = 0;
        secretNumber = (int) (Math.random() * 10 + 1);
    }

    public int getSecretNumber() {
        return secretNumber;
    }

    public void setSecretNumber(int secretNumber) {
        this.secretNumber = secretNumber;
    }

    public int getGuessNumber() {
        return guessNumber;
    }

    public void setGuessNumber(int guessNumber) {
        this.guessNumber = guessNumber;
    }
}
