package com.cejv416.game;

import java.util.Scanner;

/**
 *
 * @author Ken
 */
public class PlayGame {

    private GameBean gameBean;
    private final Scanner sc;

    public PlayGame() {
        sc = new Scanner(System.in);
        resetBean();
    }

    private void resetBean() {
        gameBean = new GameBean();
    }

    private void getInput() {
        int guess = sc.nextInt();
        gameBean.setGuessNumber(guess);
    }

    private boolean evaluateGuess() {
        boolean retVal = false;
        if (gameBean.getSecretNumber() == gameBean.getGuessNumber()) {
            retVal = true;
        }
        return retVal;
    }

    private void displayClue() {
        if (gameBean.getSecretNumber() > gameBean.getGuessNumber()) {
            System.out.println("Guess a larger number");
        } else {
            System.out.println("Guess a smaller number");
        }
    }

    private void displayYouWon() {
        for (int x = 0; x < 5; ++x) {
            System.out.println("YOU WON !!!");
        }
    }

    public void playTheGame() {
        boolean win = false;
        do {
            System.out.println("Guess a number from 1 to 10:");
            getInput();
            if (evaluateGuess()) {
                win = true;
                displayYouWon();
            } else {
                displayClue();
            }
        } while (win == false);
    }
}
